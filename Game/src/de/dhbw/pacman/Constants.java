package de.dhbw.pacman;

public class Constants {
    public static final int MAX_DIFF_MS = 50;
    public static final int UPDATE_TIMER_DELAY = 10;

    public static final int ENEMY_DIRECTION_CHANGE_CHANCE = 5; // Bigger number means smaller chance; 5 = 20% chance
    public static final double ENTITY_SPEED = 0.004; // Normal is 0.004
    public static final int NUMBER_OF_ENEMIES = 4;
    public static final int PLAYER_LIVES = 3;
    public static final int CAUGHT_COOLDOWN_TIME = 2000; // The time in which the player cant be caught after being caught in ms

    public static final int GAME_STATUS_SIZE_RATIO = 20;
    public static final int GAME_SIZE_ROUND_OFF = 100; // 100 works well for grid sizes like 20 and 25
}
