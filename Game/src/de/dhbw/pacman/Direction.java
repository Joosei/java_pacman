package de.dhbw.pacman;

public enum Direction {
    NONE(0,0),
    UP(0,-Constants.ENTITY_SPEED),
    RIGHT(Constants.ENTITY_SPEED,0),
    DOWN(0,Constants.ENTITY_SPEED),
    LEFT(-Constants.ENTITY_SPEED,0);

    private final double speedX, speedY;

    Direction(double speedX, double speedY) {
        this.speedX = speedX;
        this.speedY = speedY;
    }

    public double calcSpeedX(double size, long factor) {
        return speedX * size * factor;
    }

    public double calcSpeedY(double size, long factor) {
        return speedY * size * factor;
    }

    public double getSpeedX() {
        return speedX;
    }
}
