package de.dhbw.pacman;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class EndPanel extends JPanel implements ActionListener {

    private final GridBagConstraints gbc;

    private final ScreenActionListener actionListener;

    private JButton buttonAgain;

    private final boolean won;

    public EndPanel (ScreenActionListener actionListener, boolean won) {
        super(new GridBagLayout());
        gbc  = new GridBagConstraints();
        this.actionListener = actionListener;
        this.won = won;
        addComponents();
    }

    public void addComponents() {
        JLabel labelWon;

        if (won) labelWon = new JLabel("YOU WON!");
        else labelWon = new JLabel("YOU LOST!");

        labelWon.setFont(new Font("Trebuchet MS", Font.BOLD, 40));

        buttonAgain = new JButton("PLAY AGAIN");
        buttonAgain.setPreferredSize(new Dimension(300, 100));
        buttonAgain.setBackground(Color.DARK_GRAY);
        buttonAgain.setForeground(Color.white);
        buttonAgain.setFont(new Font("Trebuchet MS", Font.BOLD, 25));

        JButton buttonStart = new JButton("BACK TO START");
        buttonStart.setPreferredSize(new Dimension(300, 100));
        buttonStart.setBackground(Color.DARK_GRAY);
        buttonStart.setForeground(Color.white);
        buttonStart.setFont(new Font("Trebuchet MS", Font.BOLD, 25));

        gbc.insets = new Insets(10,10,10,10);

        gbc.gridx = 0;
        gbc.gridy = 0;
        add(labelWon, gbc);

        gbc.gridx = 0;
        gbc.gridy = 1;
        add(buttonAgain, gbc);

        gbc.gridx = 0;
        gbc.gridy = 2;
        add(buttonStart, gbc);

        buttonAgain.addActionListener(this);
        buttonStart.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        actionListener.screenCallback(false, e.getSource() != buttonAgain);
    }
}
