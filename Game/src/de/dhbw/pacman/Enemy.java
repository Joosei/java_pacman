package de.dhbw.pacman;

import java.io.IOException;

public class Enemy extends Entity {

    private final int serialNumber;

    public Enemy(int startX, int startY, int size, int serialNumber, StatusPanel statusPanel, ResourceCache resourceCache) {
        super(startX, startY, size, statusPanel, resourceCache);
        this.serialNumber = serialNumber;
    }

    @Override
    protected void setImage() throws IOException {
        super.setImage();

        // modulo serialNumber allows more than four enemies -> the fifth would take the skin of the first one again and so forth
        switch (serialNumber % 4) {
            case 0 -> image = resourceCache.mask;
            case 1 -> image = resourceCache.vaccine;
            case 2 -> image = resourceCache.hazmat;
            case 3 -> image = resourceCache.disinfection;
        }
    }

    @Override
    public void update(int w, int h, long diffMs, int gridSize, WorldObject[][] worldObjects) {

        if (diffMs > Constants.MAX_DIFF_MS) diffMs = Constants.MAX_DIFF_MS; // handle lags
        super.update(w, h, diffMs, gridSize, worldObjects);

        if (direction == Direction.NONE) {
            randomDirection();
            direction = directionElect;
        }

        // if wall in front change direction
        if (typeOfRectIncoming(worldObjects, diffMs, direction) == WorldObjectType.HOUSE) randomDirection();

        if (gridChangeIncoming(diffMs)) {
            int random = (int)(Math.random() * Constants.ENEMY_DIRECTION_CHANGE_CHANCE);
            if (random == 0) randomDirection();
        }
    }

    public void checkPlayerIntersection (Player player) {
        if (player.intersects(x,y)) {
            statusPanel.playerCaught();
        }
    }

    private void randomDirection() {
        int random = (int)(Math.random() * 4);

        switch (random) {
            case 0 -> directionElect = Direction.UP;
            case 1 -> directionElect = Direction.RIGHT;
            case 2 -> directionElect = Direction.DOWN;
            case 3 -> directionElect = Direction.LEFT;
        }
    }
}

