package de.dhbw.pacman;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class Entity {

    private final int startX, startY;
    protected double x, y, size;
    protected BufferedImage image;

    protected Direction direction;
    protected Direction directionElect;

    protected StatusPanel statusPanel; // This exists to have a reference to it, maybe theres a better way for this

    protected ResourceCache resourceCache;

    public Entity(int startX, int startY, double size, StatusPanel statusPanel, ResourceCache resourceCache) {
        this.startX = startX;
        this.startY = startY;
        this.size = size;

        direction = Direction.NONE;
        directionElect = Direction.NONE;
        this.statusPanel = statusPanel;
        this.resourceCache = resourceCache;

        try {
            setImage();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setDirection(Direction direction) {
        directionElect = direction;
    }

    public void update(int w, int h, long diffMs, int gridSize, WorldObject[][] worldObjects) {

        // Change direction to directionElect if with next move new 20x20 grid and rectangle in new direction is walkable, set position on exact raster coordinates
        if (direction != Direction.NONE && direction != directionElect && direction.getSpeedX() + directionElect.getSpeedX() != 0) {
            // If direction change is 90 degrees wait for gridChange and set to exact position
            if (gridChangeIncoming(diffMs) && typeOfRectIncoming(worldObjects, diffMs, directionElect) != WorldObjectType.HOUSE) {
                // first place entity on exact grid position then change direction
                switch (direction) {
                    case UP -> y = y - y % size;
                    case RIGHT -> x = x + (size - (x % size));
                    case DOWN -> y = y + (size - (y % size));
                    case LEFT -> x = x - (x % size);
                    case NONE -> {} // no repositioning needed
                    default -> throw new IllegalStateException("Unexpected value: " + direction);
                }
                direction = directionElect;
                try {
                    setImage();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        else if (direction != directionElect) {
            // only is executed when direction change is not 90 degrees
            direction = directionElect;
            try {
                setImage();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (typeOfRectIncoming(worldObjects, diffMs, direction) != WorldObjectType.HOUSE) {
            x += direction.calcSpeedX(size, diffMs);
            y += direction.calcSpeedY(size, diffMs);
        }

        // dynamic resizing depending on the smaller side of the window - might be obsolete since GamePanel size uses only the height
        if (w < h) size = w/(float)gridSize;
        else size = h/(float)gridSize;

        // if not moved set it to start
        if (direction == Direction.NONE) {
            x = startX * size;
            y = startY * size;
        }
    }

    public void paint(Graphics g) throws IOException {
        g.drawImage(image, (int)x, (int)y, (int)size, (int)size, null);
    }

    protected void setImage() throws IOException {}

    /**
     * @return Returns the WorldObjectType of the incoming rectangle
     */
    protected WorldObjectType typeOfRectIncoming(WorldObject[][] worldObjects, long diffMs, Direction direction) {
        switch (direction) {
            case UP -> {
                return worldObjects[(int) ((x + (size / 2)) / size)][(int) ((y + direction.calcSpeedY(size, diffMs)) / size)].getType();
            }
            case DOWN -> {
                return worldObjects[(int) ((x + (size / 2)) / size)][(int) ((y + direction.calcSpeedY(size, diffMs) + size) / size)].getType();
            }
            case LEFT -> {
                return worldObjects[(int) ((x + direction.calcSpeedX(size, diffMs)) / size)][(int) ((y + (size / 2)) / size)].getType();
            }
            case RIGHT -> {
                return worldObjects[(int) ((x + direction.calcSpeedX(size, diffMs) + size) / size)][(int) ((y + (size / 2)) / size)].getType();
            }
            case NONE -> {
                return WorldObjectType.HOUSE;
            }
            default -> throw new IllegalStateException("unexpected value: " + direction);
        }
    }

    /**
     * @return Returns if with the coming update which changes the position of the Entity, the Entity will be on another grid of the map
     */
    protected boolean gridChangeIncoming(long diffMs) {
        switch (direction) {
            case UP -> {
                return (y % size) < (direction.calcSpeedY(size, diffMs) * -1);
            }
            case RIGHT -> {
                return (size - (x % size)) < direction.calcSpeedX(size, diffMs);
            }
            case DOWN -> {
                return (size - (y % size)) < direction.calcSpeedY(size, diffMs);
            }
            case LEFT -> {
                return (x % size) < (direction.calcSpeedX(size, diffMs) * -1);
            }
            case NONE -> {
                return false;
            }
            default -> throw new IllegalStateException("unexpected value: " + direction);
        }
    }

    public boolean intersects(double xCheck, double yCheck) {
        return xCheck > x - size && xCheck < x + size && yCheck > y - size && yCheck < y + size;
    }

}

