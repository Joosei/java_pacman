package de.dhbw.pacman;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.Objects;

public class Game {

    private final JFrame frame;

    private GamePanel gamePanel;

    public Game() {
        // Create Window with Title and Icon
        frame = new JFrame("Pandemic Blob");
        Image icon = null;
        try {
            icon = ImageIO.read(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("graphics/player/up.png")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        frame.setIconImage(icon);
        frame.getContentPane().setBackground(Color.DARK_GRAY);

        // Make sure that the program exits when the window is closed by the user
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

        // Use BoxLayout so that size of panels can be controlled
        frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));

        startScreen();
    }

    private void startScreen() {
        frame.getContentPane().removeAll();

        // Pass object of functional interface ScreenActionListener to Panel for callback
        StartPanel startPanel = new StartPanel((won, returnToStart) -> gameScreen());

        startPanel.addComponents();
        frame.getContentPane().add(startPanel);

        frame.pack();
        frame.setVisible(true);

        frame.setExtendedState(frame.getExtendedState() | JFrame.MAXIMIZED_BOTH);

        frame.validate();
        frame.repaint();

        startPanel.setFocusable(true);
        startPanel.requestFocus();
    }

    private void gameScreen() {
        frame.getContentPane().removeAll();

        // maximize the frame to measure display height of used screen
        frame.setExtendedState(frame.getExtendedState() | JFrame.MAXIMIZED_BOTH);
        int usableWindowSize = frame.getHeight();

        // Pass object of functional interface ScreenActionListener to Panel for callback
        StatusPanel statusPanel = new StatusPanel((won, returnToStart) -> endScreen(won));

        statusPanel.setMaximumSize(new Dimension(usableWindowSize - usableWindowSize / Constants.GAME_STATUS_SIZE_RATIO, usableWindowSize / Constants.GAME_STATUS_SIZE_RATIO));
        frame.getContentPane().add(statusPanel);

        int gameWindowSize = (usableWindowSize - (usableWindowSize / Constants.GAME_STATUS_SIZE_RATIO)) - (usableWindowSize - usableWindowSize / Constants.GAME_STATUS_SIZE_RATIO) % Constants.GAME_SIZE_ROUND_OFF;

        gamePanel = new GamePanel(statusPanel);
        gamePanel.setMaximumSize(new Dimension(gameWindowSize, gameWindowSize));
        frame.getContentPane().add(gamePanel);

        frame.pack();
        frame.setVisible(true);
        frame.setExtendedState(frame.getExtendedState() | JFrame.MAXIMIZED_BOTH);

        gamePanel.setFocusable(true);
        statusPanel.setFocusable(true);
        gamePanel.requestFocus();

        frame.validate();
        frame.repaint();
        frame.setResizable(false);

        gamePanel.start();
    }

    private void endScreen(boolean playerWon) {
        gamePanel.stop();

        frame.getContentPane().removeAll();

        // Pass object of functional interface ScreenActionListener to Panel for callback
        EndPanel endPanel = new EndPanel((won, returnToStart) -> {
            if (returnToStart) startScreen();
            else gameScreen();
        }, playerWon);

        frame.getContentPane().add(endPanel);

        frame.pack();
        frame.setVisible(true);
        frame.setExtendedState(frame.getExtendedState() | JFrame.MAXIMIZED_BOTH);

        endPanel.setFocusable(true);
        endPanel.requestFocus();

        frame.validate();
        frame.repaint();
        frame.setResizable(true);
    }

}
