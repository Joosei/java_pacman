package de.dhbw.pacman;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;

public class GamePanel extends JPanel implements KeyListener{

    private final Timer updateTimer = new Timer(Constants.UPDATE_TIMER_DELAY, this::timerUpdate);
    private long previousTime;

    private final StatusPanel statusPanel;

    private WorldObject[][] worldObjects;
    private Player player;
    private Enemy[] enemies;

    public ResourceCache resourceCache;

    private int gridSize;

    public GamePanel(StatusPanel statusPanel) {
        addKeyListener(this);
        this.statusPanel = statusPanel;
    }

    private void timerUpdate(ActionEvent actionEvent) {
        long currentTime = System.currentTimeMillis();
        long diff = currentTime - previousTime;
        previousTime = currentTime;

        player.update(getWidth(), getHeight(), diff, gridSize, worldObjects);
        for (Enemy enemy: enemies) {
            enemy.update(getWidth(), getHeight(), diff, gridSize, worldObjects);
            enemy.checkPlayerIntersection(player);
        }
        repaint();
    }

    public void start() {
        initializeWorld();
    }

    public void stop() {
        updateTimer.stop();
    }

    private void initializeWorld() {
        setBackground(Color.DARK_GRAY);

        // the following just shows the text "loading map" during the loading of the map...
        JLabel labelLoading = new JLabel("Loading map......", SwingConstants.CENTER);
        labelLoading.setForeground(Color.white);
        labelLoading.setFont(new Font("Trebuchet MS", Font.PLAIN, getHeight()/20));
        labelLoading.setPreferredSize(new Dimension(getWidth(),getHeight()));
        add(labelLoading);

        // load the map in a new thread because it can take some time
        Thread readMap = new Thread(() -> {
            int population = 0, playerStartX = 0, playerStartY = 0, enemyStartX = 0, enemyStartY = 0;

            // Create and fill up the resourceCache with the images
            try {
                resourceCache = new ResourceCache();
            } catch (IOException e) {
                e.printStackTrace();
            }

            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new InputStreamReader(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("maps/map1.txt"))));

                for (int i = 0; i < 5; i++) {
                    String line = reader.readLine();
                    if (line == null) throw new IOException("file to short...");
                    String[] parts = line.split(" = ");
                    if (parts.length < 2) throw new IOException("invalid format...");

                    switch (parts[0]) {
                        case "gridSize" -> gridSize = Integer.parseInt(parts[1]);
                        case "playersX" -> playerStartX = Integer.parseInt(parts[1]);
                        case "playersY" -> playerStartY = Integer.parseInt(parts[1]);
                        case "enemiesX" -> enemyStartX = Integer.parseInt(parts[1]);
                        case "enemiesY" -> enemyStartY = Integer.parseInt(parts[1]);
                    }
                }

                worldObjects = new WorldObject[gridSize][gridSize];

                for (int i = 0; i < gridSize; i++) {
                    String str = reader.readLine();
                    for (int j = 0; j < gridSize; j++) {
                        switch (str.charAt(j)) {
                            case 'h' -> worldObjects[j][i] = new WorldObject(j * (getHeight() / gridSize), i * (getHeight() / gridSize), WorldObjectType.HOUSE, resourceCache);
                            case 'p' -> {
                                worldObjects[j][i] = new WorldObject(j * (getHeight() / gridSize), i * (getHeight() / gridSize), WorldObjectType.PERSON, resourceCache);
                                population++;
                            }
                            case 'r' -> worldObjects[j][i] = new WorldObject(j * (getHeight() / gridSize), i * (getHeight() / gridSize), WorldObjectType.ROAD, resourceCache);
                        }
                    }
                }
            } catch (IOException ex) {
                System.out.println("Error reading map file: " + ex.getMessage());
            } finally {
                try {
                    if (reader != null)
                        reader.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }

            statusPanel.setPopulation(population);

            player = new Player(playerStartX, playerStartY, 100, statusPanel, resourceCache);

            enemies = new Enemy[Constants.NUMBER_OF_ENEMIES]; // TODO: let the player choose the number of enemies
            for (int i = 0; i < Constants.NUMBER_OF_ENEMIES; i++) {
                enemies[i] = new Enemy(enemyStartX, enemyStartY,100, i, statusPanel, resourceCache);
            }

            System.out.println("World initialized; Starting Game...");
            remove(labelLoading);

            // After successfully loading the world the timer can start
            updateTimer.start();
        });

        readMap.start();
    }

    private void paintWorld(Graphics g) {
        for (int i = 0; i < gridSize; i++) {
            for (int j = 0; j < gridSize; j++) {
                try {
                    worldObjects[i][j].paint(g, getWidth(), getHeight(), gridSize);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        // Only when the world is fully initialized and the timer is running shall thy world be painted
        if (updateTimer.isRunning()) {
            paintWorld(g);
            try {
                player.paint(g);

                for (Enemy enemy: enemies) {
                    enemy.paint(g);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void keyPressed (KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_W, KeyEvent.VK_UP -> player.setDirection(Direction.UP);
            case KeyEvent.VK_A, KeyEvent.VK_LEFT -> player.setDirection(Direction.LEFT);
            case KeyEvent.VK_S, KeyEvent.VK_DOWN -> player.setDirection(Direction.DOWN);
            case KeyEvent.VK_D, KeyEvent.VK_RIGHT -> player.setDirection(Direction.RIGHT);
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }
}