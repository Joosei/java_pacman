package de.dhbw.pacman;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.IOException;

public class Player extends Entity {

    private final Timer caughtTimer = new Timer(Constants.CAUGHT_COOLDOWN_TIME, this::timerUpdate);

    public Player(int startX, int startY, int size, StatusPanel statusPanel, ResourceCache resourceCache) {
        super(startX,startY,size, statusPanel, resourceCache);
    }

    @Override
    protected void setImage() throws IOException {
        super.setImage();

        if (caughtTimer != null && caughtTimer.isRunning()) {
            switch (direction) {
                case NONE, UP -> image = resourceCache.up_caught;
                case RIGHT -> image = resourceCache.right_caught;
                case DOWN -> image = resourceCache.down_caught;
                case LEFT -> image = resourceCache.left_caught;
                default -> throw new IllegalStateException("Unexpected value: " + direction);
            }
        }
        else {
            switch (direction) {
                case NONE, UP -> image = resourceCache.up;
                case RIGHT -> image = resourceCache.right;
                case DOWN -> image = resourceCache.down;
                case LEFT -> image = resourceCache.left;
                default -> throw new IllegalStateException("Unexpected value: " + direction);
            }
        }
    }

    @Override
    public void update(int w, int h, long diffMs, int gridSize, WorldObject[][] worldObjects) {

        if (diffMs > Constants.MAX_DIFF_MS) diffMs = Constants.MAX_DIFF_MS; // handle lags
        super.update(w, h, diffMs, gridSize, worldObjects);

        if (worldObjects[(int) ((x + (size / 2)) / size)][(int) ((y + (size / 2)) / size)].getType() == WorldObjectType.PERSON) {

            worldObjects[(int) ((x + (size / 2)) / size)][(int) ((y + (size / 2)) / size)].setType(WorldObjectType.ROAD);

            try {
                worldObjects[(int) ((x + (size / 2)) / size)][(int) ((y + (size / 2)) / size)].setImage();
            } catch (IOException e) {
                e.printStackTrace();
            }

            statusPanel.incrementScore();
        }
    }

    private void timerUpdate(ActionEvent actionEvent) {
        caughtTimer.stop();
        try {
            setImage();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean intersects(double xCheck, double yCheck) {
        // The timer enables a 2 second delay when the player has been caught
        if (!caughtTimer.isRunning() && super.intersects(xCheck, yCheck))
        {
            caughtTimer.start();
            try {
                setImage();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        }
        else return false;
    }
}