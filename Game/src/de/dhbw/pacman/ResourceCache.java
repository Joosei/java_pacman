package de.dhbw.pacman;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;

public class ResourceCache {

    public final BufferedImage up;
    public final BufferedImage right;
    public final BufferedImage down;
    public final BufferedImage left;

    public final BufferedImage up_caught;
    public final BufferedImage right_caught;
    public final BufferedImage down_caught;
    public final BufferedImage left_caught;

    public final BufferedImage mask;
    public final BufferedImage vaccine;
    public final BufferedImage hazmat;
    public final BufferedImage disinfection;

    public final BufferedImage house0;
    public final BufferedImage house1;
    public final BufferedImage house2;
    public final BufferedImage road;

    public final BufferedImage[][] persons;

    public ResourceCache() throws IOException {
        up = ImageIO.read(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("graphics/player/up.png")));
        right = ImageIO.read(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("graphics/player/right.png")));
        down = ImageIO.read(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("graphics/player/down.png")));
        left = ImageIO.read(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("graphics/player/left.png")));

        up_caught = ImageIO.read(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("graphics/player/up_caught.png")));
        right_caught = ImageIO.read(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("graphics/player/right_caught.png")));
        down_caught = ImageIO.read(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("graphics/player/down_caught.png")));
        left_caught = ImageIO.read(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("graphics/player/left_caught.png")));

        mask = ImageIO.read(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("graphics/enemy/mask.png")));
        vaccine = ImageIO.read(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("graphics/enemy/vaccine.png")));
        hazmat = ImageIO.read(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("graphics/enemy/hazmat.png")));
        disinfection = ImageIO.read(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("graphics/enemy/disinfection.png")));

        house0 = ImageIO.read(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("graphics/map/house0.png")));
        house1 = ImageIO.read(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("graphics/map/house1.png")));
        house2 = ImageIO.read(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("graphics/map/house2.png")));
        road = ImageIO.read(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("graphics/map/road.png")));

        persons = new BufferedImage[4][24];

        for (int i = 0; i < 4; i++) {
            String str = "";
            switch (i) {
                case 0 -> str = "up";
                case 1 -> str = "right";
                case 2 -> str = "down";
                case 3 -> str = "left";
            }
            for (int j = 0; j < 24; j++) {
                persons[i][j] = ImageIO.read(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("graphics/map/person/" + str + "/person" + j + ".png")));
            }
        }
    }
}
