package de.dhbw.pacman;

public interface ScreenActionListener {
    void screenCallback(boolean won, boolean returnToStart);
}
