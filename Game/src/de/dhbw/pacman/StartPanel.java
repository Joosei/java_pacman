package de.dhbw.pacman;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class StartPanel extends JPanel implements ActionListener {

    private final GridBagConstraints gbc;

    private final ScreenActionListener actionListener;

    public StartPanel(ScreenActionListener actionListener) {
        super(new GridBagLayout());
        gbc  = new GridBagConstraints();
        this.actionListener =  actionListener;
    }

    public void addComponents() {
        // TODO: highscores, maps, player name

        JLabel labelStart1 = new JLabel("WELCOME TO PANDEMIC BLOB");
        labelStart1.setFont(new Font("Trebuchet MS", Font.BOLD, 60));

        JLabel labelStart2 = new JLabel("- THE MOST INFECTIOUS PAC-MAN EVER -");
        labelStart2.setFont(new Font("Trebuchet MS", Font.PLAIN, 30));

        JButton buttonStart = new JButton("START GAME");
        buttonStart.setPreferredSize(new Dimension(300, 100));
        buttonStart.setBackground(Color.DARK_GRAY);
        buttonStart.setForeground(Color.white);
        buttonStart.setFont(new Font("Trebuchet MS", Font.BOLD, 25));

        JLabel labelStart3 = new JLabel("You are the Coronavirus!");
        JLabel labelStart4 = new JLabel("After taking the step of being transmissible to humankind, you must now infect the entire population.");
        JLabel labelStart5 = new JLabel("But beware: Humanity has introduced several measures to bring you down!");
        JLabel labelStart6 = new JLabel("Control the virus with WASD or the Arrow-Keys. Good luck!");
        labelStart3.setFont(new Font("Trebuchet MS", Font.PLAIN, 20));
        labelStart4.setFont(new Font("Trebuchet MS", Font.PLAIN, 20));
        labelStart5.setFont(new Font("Trebuchet MS", Font.PLAIN, 20));
        labelStart6.setFont(new Font("Trebuchet MS", Font.PLAIN, 20));

        gbc.insets = new Insets(10,10,10,10);

        gbc.gridx = 0;
        gbc.gridy = 0;
        add(labelStart1, gbc);

        gbc.gridx = 0;
        gbc.gridy = 1;
        add(labelStart2, gbc);

        gbc.gridx = 0;
        gbc.gridy = 2;
        add(buttonStart, gbc);

        gbc.insets = new Insets(30,10,0,10);

        gbc.gridx = 0;
        gbc.gridy = 3;
        add(labelStart3, gbc);

        gbc.insets = new Insets(3,10,0,10);

        gbc.gridx = 0;
        gbc.gridy = 4;
        add(labelStart4, gbc);

        gbc.gridx = 0;
        gbc.gridy = 5;
        add(labelStart5, gbc);

        gbc.gridx = 0;
        gbc.gridy = 6;
        add(labelStart6, gbc);

        buttonStart.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        actionListener.screenCallback(false, false);
    }

}
