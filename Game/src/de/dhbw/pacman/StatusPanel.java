package de.dhbw.pacman;

import javax.swing.*;
import java.awt.*;

public class StatusPanel extends JPanel {

    private final GridBagConstraints gbc;

    private final ScreenActionListener actionListener;

    private int lives, score, population;

    private JProgressBar progress;
    private JLabel labelLives;

    /**
     * StatusPanel manages and shows the player stats like lives and progress.
     * It also handles the event of the player winning or losing.
     */
    public StatusPanel(ScreenActionListener actionListener) {
        super(new GridBagLayout());
        gbc  = new GridBagConstraints();
        this.actionListener = actionListener;

        score = 0;
        lives = Constants.PLAYER_LIVES;

        addComponents();
    }

    private void addComponents() {
        setBackground(Color.DARK_GRAY);

        labelLives = new JLabel("Extra Lives: " + lives);
        labelLives.setForeground(Color.white);
        labelLives.setFont(new Font("Trebuchet MS", Font.BOLD, 15));

        JLabel labelProgress = new JLabel("Infected Population: ");
        labelProgress.setForeground(Color.white);
        labelProgress.setFont(new Font("Trebuchet MS", Font.BOLD, 15));

        progress = new JProgressBar(0, 100);
        progress.setValue(0);
        progress.setStringPainted(true);

        gbc.insets = new Insets(0,0,0,50);

        gbc.gridx = 0;
        gbc.gridy = 0;
        add(labelLives, gbc);

        gbc.insets = new Insets(0,0,0,5);

        gbc.gridx = 1;
        gbc.gridy = 0;
        add(labelProgress, gbc);

        gbc.gridx = 2;
        gbc.gridy = 0;
        add(progress, gbc);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
    }

    public void incrementScore () {
        score++;
        progress.setValue((int) ((score / (float)population) * 100));
        repaint();

        if (score == population) {
            actionListener.screenCallback(true,false);
        }
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public void playerCaught() {
        lives--;
        labelLives.setText("Extra Lives: " + lives + "          ");
        repaint();

        if (lives < 0) {
            actionListener.screenCallback(false,false);
        }
    }
}
