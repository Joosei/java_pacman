package de.dhbw.pacman;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class WorldObject {

    private final double x, y;
    private double size;
    private BufferedImage image;
    private WorldObjectType type;

    private final ResourceCache resourceCache;

    public WorldObject(int x, int y, WorldObjectType type, ResourceCache resourceCache) {
        this.x = x;
        this.y = y;
        this.size = 0;
        this.type = type;

        this.resourceCache = resourceCache;

        try {
            setImage();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void paint(Graphics g, int w, int h, int gridSize) throws IOException {
        // dynamic resizing depending on the smaller side of the window
        if (w < h) size = w/(float)gridSize;
        else size = h/(float)gridSize;

        g.drawImage(image, (int)x, (int)y, (int)size, (int)size, null);
    }

    public void setType(WorldObjectType type) {
        this.type = type;
    }

    public WorldObjectType getType() {
        return type;
    }

    public void setImage() throws IOException {
        switch (type) {
            case HOUSE -> {
                switch ((int)(Math.random() * 3)) {
                    case 0 -> image = resourceCache.house0;
                    case 1 -> image = resourceCache.house1;
                    case 2 -> image = resourceCache.house2;
                }
            }
            case PERSON -> image = resourceCache.persons[(int)(Math.random() * 4)][(int)(Math.random() * 24)];
            case ROAD -> image = resourceCache.road;
        }
    }

}
