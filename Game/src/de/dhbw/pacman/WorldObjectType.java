package de.dhbw.pacman;

public enum WorldObjectType {
    HOUSE,
    PERSON,
    ROAD
}
